import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class User{
  
  final String firstName;
  final String lastName;
  final String userId;
  
}